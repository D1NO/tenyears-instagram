require "rubygems"
require 'rack/reverse_proxy'

use Rack::ReverseProxy do 
  reverse_proxy_options :preserve_host => true
  reverse_proxy '/v1', 'https://api.instagram.com/v1'
end
use Rack::Static, 
  :urls => ["/css", "/js", "/img"],
  :root => "public"

run lambda { |env|
  [
    200, 
    {
      'Content-Type'  => 'text/html', 
      'Cache-Control' => 'public, max-age=86400' 
    },
    File.open('public/index.html', File::RDONLY)
  ]
}